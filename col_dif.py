import pandas as pd

path = "/home/hank/Work/GnD/Development/Carel/RetainLog.csv"

df = pd.read_csv(path)

"""
Need to figure out how to adjust the output of 
the value_counts here so I can give it all as a percentage
install of decimals.
Couldn't figure that out.
"""
for col in df.columns[2:]:
    if df[col].nunique() > 1:
        ddf = pd.DataFrame()
        val = df[f'{col}'].value_counts(normalize=True).to_frame()
        ddf = ddf.append(val)
        print(ddf,"\n",file=open("carel_output.txt", "a", encoding="utf-8"))
        #if val > 0.0:
        #    print(df[f'{col}'].value_counts())
        #else:
        #    pass
print("Different columns output into carel_output.txt")
